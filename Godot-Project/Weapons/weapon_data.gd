class_name WeaponData
extends Reference

var _damage_spread = -1
var _base_damage = -1
var _level_damage_mod = -1

var _cooldown_frames = -1
var _level = 0
var _levelup_attribute_changes = []

func get_cooldown_frames():
	return _cooldown_frames

func _set_cooldown_seconds(val : float):
	_cooldown_frames = floor(val * 60)

func get_base_damage() -> int:
	return _base_damage + _level_damage_mod

func _set_base_damage(val : int):
	_base_damage = val

func _set_damage_spread(val : int):
	_damage_spread = val

func get_level() -> int:
	return _level

func set_level(val : int):
	assert(val > 0 and val <= _levelup_attribute_changes.size())
	_level = val
	_refresh_level_damage_mod()

func _refresh_level_damage_mod():
	_level_damage_mod = 0
	for i in range(_level):
		var current_level_changes = _levelup_attribute_changes[i]
		var change_at_this_level = current_level_changes.get("damage", 0)
		_level_damage_mod += change_at_this_level
	
	print("The current level damage modifier is: %s" % _level_damage_mod)

func roll_damage():
	return (randi() % _damage_spread) + get_base_damage()

func load_from_json(path : String):
	var file = File.new()
	file.open("res://Weapons/Weapon-Data/" + path + ".json", File.READ)
	var json_data = parse_json(file.get_as_text())
	_set_base_damage(json_data.get("base_damage", -1))
	_set_damage_spread(json_data.get("damage_spread", -1))
	_set_cooldown_seconds(json_data.get("base_cooldown", -1))
	_levelup_attribute_changes = json_data.get("levelup_attribute_changes")
	set_level(1)
