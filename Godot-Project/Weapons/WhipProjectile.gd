class_name WhipProjectile
extends Node2D

const SCENE_PATH = "res://Weapons/WhipProjectile.tscn"
const ACTIVE_FRAMES = 6

var _remaining_active_frames = -1
var _active = false
var _weapon_data : WeaponData = null

static func instance():
	var inst = load(SCENE_PATH).instance()
	return inst

func set_weapon_data(val : WeaponData):
	_weapon_data = val

func fire():
	assert(_weapon_data != null, "A weapon needs weapon data to fire.")
	_remaining_active_frames = ACTIVE_FRAMES
	visible = true
	_active = true

func _disable():
	_remaining_active_frames = 0
	visible = false
	_active = false

func _physics_process(_delta):
	if _remaining_active_frames > 0:
		_remaining_active_frames -= 1
		_deal_damage()
		if _remaining_active_frames <= 0:
			_disable()

func _deal_damage():
	var damage = _weapon_data.roll_damage()
	print("Whip deals %s damage!" % damage)

func is_active():
	return _active
