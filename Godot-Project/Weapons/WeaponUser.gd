extends Node2D

var _frames_to_attack = 0
var _projectiles = []
var _weapon_data : WeaponData

func _get_cooldown() -> int:
	return _weapon_data.get_cooldown_frames()

func _start_cooldown():
	_frames_to_attack = _get_cooldown()

func _ready():
	_weapon_data = WeaponData.new()
	_weapon_data.load_from_json("whip")
	_weapon_data.set_level(4)
	_start_cooldown()

func _physics_process(_delta):
	_frames_to_attack -= 1
	
	if _frames_to_attack <= 0:
		_use_weapon()
		_start_cooldown()

func _use_weapon():
	# This would look very different for weapons like Garlic and will be rewritten when we get there.
	_fire_next_projectile()

func _fire_next_projectile():
	var current_projectile = null
	# We use an object pooling system to better support amount increases and rapid fire weapons like wand when we get there.
	for projectile in _projectiles:
		if not projectile.is_active():
			current_projectile = projectile
	
	if current_projectile == null:
		current_projectile = _create_new_projectile()
		current_projectile.set_weapon_data(_weapon_data)
	
	current_projectile.fire()

func _create_new_projectile():
	var projectile = WhipProjectile.instance()
	_projectiles.append(projectile)
	add_child(projectile)
	return projectile
